<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/accessdenied', 'HomeController@denied')->name('access.denied');
Route::get('getjob/{id}', 'PartController@getJob');
Route::get('getmerek/{merek}', 'PartController@getMerek');
Route::get('getmodel/{model}', 'PartController@getModel');
Route::post('login','AuthController@login')->name('login');

Route::group(['middleware' => ['Login']], function () {
    Route::group(['prefix' => 'part'], function () {
        Route::get('/' ,['as' => 'part' ,'uses' => 'PartController@index']);
        Route::get('/create', ['as' => 'part.create', 'uses' => 'PartController@create']);
        Route::post('/store', ['as' => 'part.store', 'uses' => 'PartController@store']);
        Route::get('/edit/{id}',['as' => 'part.edit', 'uses' => 'PartController@edit']);
        Route::PATCH('/update/{id}', ['as' => 'part.update' , 'uses' => 'PartController@update']);
        Route::get('/delete/{id}',['as' => 'part.delete', 'uses' => 'PartController@delete']);
    });

    Route::group(['prefix' => 'job'], function () {
        Route::get('/' ,['as' => 'job' ,'uses' => 'JobController@index']);
        Route::get('/create', ['as' => 'job.create', 'uses' => 'JobController@create']);
        Route::post('/store', ['as' => 'job.store', 'uses' => 'JobController@store']);
        Route::get('/edit/{id}',['as' => 'job.edit', 'uses' => 'JobController@edit']);
        Route::PATCH('/update/{id}', ['as' => 'job.update' , 'uses' => 'JobController@update']);
        Route::get('/delete/{id}',['as' => 'job.delete', 'uses' => 'JobController@delete']);
    });

    Route::group(['prefix' => 'brand'], function () {
        Route::get('/' ,['as' => 'brand' ,'uses' => 'BrandController@index']);
        Route::get('/create', ['as' => 'brand.create', 'uses' => 'BrandController@create']);
        Route::post('/store', ['as' => 'brand.store', 'uses' => 'BrandController@store']);
        Route::get('/edit/{id}',['as' => 'brand.edit', 'uses' => 'BrandController@edit']);
        Route::PATCH('/update/{id}', ['as' => 'brand.update' , 'uses' => 'BrandController@update']);
        Route::get('/delete/{id}',['as' => 'brand.delete', 'uses' => 'BrandController@delete']);
    });

    Route::group(['prefix' => 'kelas'], function () {
        Route::get('/' ,['as' => 'kelas' ,'uses' => 'KelasController@index']);
        Route::get('/create', ['as' => 'kelas.create', 'uses' => 'KelasController@create']);
        Route::post('/store', ['as' => 'kelas.store', 'uses' => 'KelasController@store']);
        Route::get('/edit/{id}',['as' => 'kelas.edit', 'uses' => 'KelasController@edit']);
        Route::PATCH('/update/{id}', ['as' => 'kelas.update' , 'uses' => 'KelasController@update']);
        Route::get('/delete/{id}',['as' => 'kelas.delete', 'uses' => 'KelasController@delete']);
    });

    Route::group(['prefix' => 'mobil'], function () {
        Route::get('/' ,['as' => 'mobil' ,'uses' => 'MobilController@index']);
        Route::get('/create', ['as' => 'mobil.create', 'uses' => 'MobilController@create']);
        Route::post('/store', ['as' => 'mobil.store', 'uses' => 'MobilController@store']);
        Route::get('/edit/{id}',['as' => 'mobil.edit', 'uses' => 'MobilController@edit']);
        Route::PATCH('/update/{id}', ['as' => 'mobil.update' , 'uses' => 'MobilController@update']);
        Route::get('/delete/{id}',['as' => 'mobil.delete', 'uses' => 'MobilController@delete']);
    });
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/pilih/{id}', 'HomeController@pilih')->name('alterpart.pilih');
