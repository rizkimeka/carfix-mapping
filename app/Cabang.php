<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    protected $table = 't_cabang';
    public $timestamps = false;

    protected $guarded = [];
}
