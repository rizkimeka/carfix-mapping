<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 't_class';
    public $timestamps = false;

    protected $guarded = [];

    public function part()
    {
        return $this->hasOne('App\Part', 'kode_kelas', 'kelas');
    }
}
