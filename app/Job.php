<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 't_job';
    public $timestamps = false;

    protected $guarded = [];

    public function part()
    {
        return $this->hasOne('App\Job', 'kode_job', 'kode_job');
    }
}
