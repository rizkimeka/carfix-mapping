<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 't_brand';
    public $timestamps = false;

    protected $guarded = [];
}
