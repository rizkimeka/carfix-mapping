<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    protected $table = 't_part';
    public $timestamps = false;

    protected $guarded = [];

    public function car()
    {
        return $this->hasOne('App\Car', 'kode_mobil', 'kode_mobil');
    }

    public function job()
    {
        return $this->hasOne('App\Job', 'kode_job', 'kode_job');
    }

    public function kelas() {
        return $this->hasOne('App\Kelas','kelas','kode_kelas');
    }
}
