<?php

namespace App\Http\Controllers;
use App\Kelas;

use Illuminate\Http\Request;

class KelasController extends Controller
{
    public function index()
    {
        return view('master.kelas.index', [
            'data'  => Kelas::get(),
                // 'datas' => Alterpart::where('status', 2)->orWhere('status', 1)->get(),
            ]);
    }

    public function create()
    {
        return view('master.kelas.create', [
            'data'  => Kelas::get(),
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_kelas' => 'unique:t_class',
             ]);
        $store = Kelas::create($request->all());
        return redirect()->route('kelas')->with('message', 'Success, Data Berhasil Ditambahkan');
    }

    public function edit($id)
    {
        $data = Kelas::where('id',$id)->first();
        return view('master.kelas.edit',compact('data'));
    }

    public function update(Request $request,$id)
    {
        $edit = Kelas::where('id', $id)->update([
            'kode_kelas'     => $request->kode_kelas,
            'nama_kelas'     => $request->nama_kelas,
        ]);
        return redirect('kelas')->with('message', 'Success, Data Berhasil Diperbarui');
    }

    public function delete($id){
        $delete = Kelas::where('id',$id)->delete();

        if ($delete) {
            return redirect()->route('kelas')->with('message','Berhasil Terhapus');
        }else{
            return redirect()->route('kelas')->with('danger','Gagal Terhapus');
        }
    }
}
