<?php

namespace App\Http\Controllers;
use App\Job;

use Illuminate\Http\Request;

class JobController extends Controller
{
    public function index()
    {
        return view('master.job.index', [
            'data'  => Job::get(),
                // 'datas' => Alterpart::where('status', 2)->orWhere('status', 1)->get(),
            ]);
    }

    public function create()
    {
        return view('master.job.create', [
            'data'  => Job::get(),
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_job' => 'unique:t_job',
             ]);
        $store = Job::create($request->all());
        return redirect()->route('job')->with('message', 'Success, Data Berhasil Ditambahkan');
    }

    public function edit($id)
    {
        $data = Job::where('id',$id)->first();
        return view('master.job.edit',compact('data'));
    }

    public function update(Request $request,$id)
    {
        $edit = Job::where('id', $id)->update([
            'kode_job'     => $request->kode_job,
            'nama_job'     => $request->nama_job,
            'flatrate'     => $request->flatrate,
        ]);
        return redirect('job')->with('message', 'Success, Data Berhasil Diperbarui');
    }

    public function delete($id){
        $delete = Job::where('id',$id)->delete();

        if ($delete) {
            return redirect()->route('job')->with('message','Berhasil Terhapus');
        }else{
            return redirect()->route('job')->with('danger','Gagal Terhapus');
        }
    }
}
