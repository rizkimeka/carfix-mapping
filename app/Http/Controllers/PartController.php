<?php

namespace App\Http\Controllers;
use App\Mapping;
use App\Car;
use App\Job;
use App\Kelas;
use App\Mobil;
use App\Part;
use App\User;
use auth;

use Illuminate\Http\Request;

class PartController extends Controller
{
    public function index()
    {
        if(auth::user()->hak_akses == 1){
            return view('part.index', [
                'data'  => Mapping::get(),
                // 'datas' => Alterpart::where('status', 2)->orWhere('status', 1)->get(),
            ]);
        } else if(auth::user()->hak_akses == 2) {
            $data = Mapping::get();
            return view('part.index')->with('data',$data);
        }
    }

    public function create()
    {
        if(auth::user()->hak_akses == 1){
                return view('part.create', [
                'datas' => Mapping::get(),
                'job' => Job::get(),
                'mobil' => Mobil::groupBy('merek')->get(),
                'model' => Mobil::groupBy('model')->get(),
                'varian' => Mobil::groupBy('varian')->get(),
            ]);
        } else if(auth::user()->hak_akses == 2) {
                return view('part.create', [
                'datas' => Mapping::get(),
                'job' => Job::get(),
                'mobil' => Mobil::groupBy('merek')->get(),
                'model' => Mobil::groupBy('model')->get(),
                'varian' => Mobil::groupBy('varian')->get(),
            ]);
        }
    }

    public function store(Request $request)
    {
        if(auth::user()->hak_akses == 1){
            $this->validate($request, [
                    // 'kode_supplier' => 'unique:tb_barang',
                    'kode_part' => 'unique:t_mapping',
                ]);
                $store = Mapping::create($request->all());
                $save = Part::create([
                    'kode_part' => $request->kode_part,
                    'nama_part' => $request->nama_part,
                    'kode_job'  => $request->kode_job,
                    'kelas'     => $request->kelas_job,
                    'kode_mobil'=> $request->kode_mobil,
                ]);
                // $store = Mbarang::create($request->all());
                return redirect()->route('part')->with('message', 'Success, Data Berhasil Ditambahkan');
        } else if(auth::user()->hak_akses == 2) {
            $this->validate($request, [
                // 'kode_supplier' => 'unique:tb_barang',
                'kode_part' => 'unique:t_mapping',
            ]);
                // $save = Mapping::where('kode_part', '=',$request->kode_part)->firstOrFail();
                // $save->update($request->all());
                // return back()->withMessage('Success');
                $store = Mapping::create($request->all());
                $save = Part::create([
                    'kode_part' => $request->kode_part,
                    'nama_part' => $request->nama_part,
                    'kode_mobil'=> $request->kode_mobil,
                    'kode_job'  => $request->kode_job,
                    'kelas'     => $request->kelas_job,
                ]);
                return redirect()->route('part')->with('message', 'Success, Data Berhasil Ditambahkan');
        }
    }

    public function edit($id)
    {
        if(auth::user()->hak_akses == 1){
            $data = Mapping::where('id',$id)->first();
            $datas = Mapping::get();
            $job = Job::get();
            $mobil = Mobil::groupBy('merek')->get();
            $model = Mobil::groupBy('model')->get();
            $varian = Mobil::groupBy('varian')->get();
            return view('part.edit',compact('data', 'datas', 'job', 'mobil', 'model', 'varian'));
        } else if(auth::user()->hak_akses == 2) {
            $data = Mapping::where('id',$id)->first();
            $datas = Mapping::get();
            $job = Job::get();
            $mobil = Mobil::groupBy('merek')->get();
            $model = Mobil::groupBy('model')->get();
            $varian = Mobil::groupBy('varian')->get();
            return view('part.edit',compact('data', 'datas', 'job', 'mobil', 'model', 'varian'));
        }
    }

    public function update(Request $request,$id)
    {
        if(auth::user()->hak_akses == 1){
            $edit = Mapping::where('id', $id)->update([
                'nama_part' => $request->nama_part,
                'brand'     => $request->brand,
                'kode_job'     => $request->kode_job,
                'nama_job'     => $request->nama_job,
                'kelas_job'     => $request->kelas_job,
                'kode_mobil'     => $request->kode_mobil,
                'merek'     => $request->merek,
                'model'     => $request->model,
                'tahun'     => $request->tahun,
                'type'       => $request->type,
            ]);

            $ubah = Part::where('kode_part', $request->kode_part)->update([
                'kode_part' => $request->kode_part,
                'nama_part' => $request->nama_part,
                'kode_mobil'=> $request->kode_mobil,
                'kode_job'  => $request->kode_job,
                'kelas'     => $request->kelas_job,
            ]);
        } else if(auth::user()->hak_akses == 2) {
            $edit = Mapping::where('id', $id)->update([
                'nama_part' => $request->nama_part,
                'brand'     => $request->brand,
                'kode_job'     => $request->kode_job,
                'nama_job'     => $request->nama_job,
                'kelas_job'     => $request->kelas_job,
                'merek'     => $request->merek,
                'model'     => $request->model,
                'tahun'     => $request->tahun,
                'type'       => $request->type,
                // 'tanggal' => $request->tanggal,
                // 'harga'     => $request->harga,
                // 'status' => $request->status,
            ]);

            $ubah = Part::where('kode_part', $request->kode_part)->update([
                'kode_part' => $request->kode_part,
                'nama_part' => $request->nama_part,
                'kode_mobil'=> $request->kode_mobil,
                'kode_job'  => $request->kode_job,
                'kelas'     => $request->kelas_job,
            ]);
        }
        return redirect('part')->with('message', 'Success, Data Berhasil Diperbarui');
    }

    public function delete($id){
        if(auth::user()->hak_akses == 1){
            $delete = Mapping::where('id',$id)->delete();

            if ($delete) {
                return redirect()->route('part')->with('message','Berhasil Terhapus');
            }else{
                return redirect()->route('part')->with('danger','Gagal Terhapus');
            }
        } else if(auth::user()->hak_akses == 2) {
            $delete = Mapping::where('id',$id)->delete();

            if ($delete) {
                return redirect()->route('part')->with('message','Berhasil Terhapus');
            }else{
                return redirect()->route('part')->with('danger','Gagal Terhapus');
            }
        }
    }

    public function getJob($id)
    {
        if(auth::user()->hak_akses == 1){
            $data = Job::where('nama_job', $id)->get();
            return response()->json($data);
        } else if(auth::user()->hak_akses == 2) {
            $data = Job::where('nama_job', $id)->get();
            return response()->json($data);
        }
    }

    public function getMerek($merek)
    {
        if(auth::user()->hak_akses == 1){
            $data = Mobil::where('merek', $merek)->groupBy('model')->get();
            return response()->json($data);
        } else if(auth::user()->hak_akses == 2) {
            $data = Mobil::where('merek', $merek)->groupBy('model')->get();
            return response()->json($data);
        }
    }

    public function getModel($model)
    {
        if(auth::user()->hak_akses == 1){
            $data = Mobil::where('model', $model)->groupBy('varian')->get();
            return response()->json($data);
        } else if(auth::user()->hak_akses == 2) {
            $data = Mobil::where('model', $model)->groupBy('varian')->get();
            return response()->json($data);
        }
    }
}
