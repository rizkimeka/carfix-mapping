<?php

namespace App\Http\Controllers;
use App\Car;
use App\Mobil;

use Illuminate\Http\Request;

class MobilController extends Controller
{
    public function index()
    {
        return view('master.mobil.index', [
            'data'  => Car::get(),
                // 'datas' => Alterpart::where('status', 2)->orWhere('status', 1)->get(),
            ]);
    }

    public function create()
    {
        return view('master.mobil.create', [
            'data'  => Car::get(),
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_mobil' => 'unique:t_car',
            'kode_mobil' => 'unique:t_mobil',
             ]);
        $store = Car::create($request->all());
        $save = Mobil::create([
            'kode_mobil' => $request->kode_mobil,
            'merek'      => $request->merek,
            'model'      => $request->model,
            'varian'     => $request->type,
            'tahun'      => $request->tahun,
        ]);
        return redirect()->route('mobil')->with('message', 'Success, Data Berhasil Ditambahkan');
    }

    public function edit($id)
    {
        $data = Car::where('id',$id)->first();
        return view('master.mobil.edit',compact('data'));
    }

    public function update(Request $request,$id)
    {
        $edit = Car::where('id', $id)->update([
            'kode_mobil'     => $request->kode_mobil,
            'merek'     => $request->merek,
            'model'     => $request->model,
            'tahun'     => $request->tahun,
            'type'      => $request->type,
        ]);

        $ubah = Mobil::where('kode_mobil', $request->kode_mobil)->update([
            'kode_mobil' => $request->kode_mobil,
            'merek'      => $request->merek,
            'model'      => $request->model,
            'varian'     => $request->type,
            'tahun'      => $request->tahun,
        ]);
        return redirect('mobil')->with('message', 'Success, Data Berhasil Diperbarui');
    }

    public function delete($id){
        $delete = Car::where('id',$id)->delete();

        if ($delete) {
            return redirect()->route('mobil')->with('message','Berhasil Terhapus');
        }else{
            return redirect()->route('mobil')->with('danger','Gagal Terhapus');
        }
    }
}
