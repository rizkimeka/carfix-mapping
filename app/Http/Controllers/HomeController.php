<?php

namespace App\Http\Controllers;

use App\Mapping;
use App\Job;
use App\Mobil;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', [
            'data'  => Mapping::get(),
            'datas' => Mobil::get(),
        ]);
    }

    // public function create()
    // {
    //     return view('Part', [
    //         'datas' => Mapping::get(),
    //         'job' => Job::get(),
    //         'mobil' => Mobil::groupBy('merek')->get(),
    //         'model' => Mobil::groupBy('model')->get(),
    //         'varian' => Mobil::groupBy('varian')->get(),
    //     ]);
    // }

    // public function store(Request $request)
    // {
    //     // $this->validate($request, [
    //     //     'kode_part' => 'unique:tb_Part',
    //     // ]);
    //     $save = Part::create($request->all());
    //     return back()->withMessage('Success');
    //     return redirect()->route('Part')->with('message', 'Success, Data Berhasil Tersimpan');
    // }

    // public function delete($id){
    //     $delete = Mapping::where('id',$id)->delete();

    //         if ($delete) {
    //             return redirect()->route('home')->with('message','Berhasil Terhapus');
    //         }else{
    //             return redirect()->route('home')->with('danger','Gagal Terhapus');
    //         }
    // }

    // public function pilih($id)
    // {
    //     Part::where('kode_part',$id)->update([
    //         'status' => '3'
    //     ]);
    //     return redirect('Car')->with('message', 'Request Dipilih');
    // }

    public function denied()
    {
        return view('accessdenied');
    }
}
