<?php

namespace App\Http\Controllers;
use App\Brand;

use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
        return view('master.brand.index', [
            'data'  => Brand::get(),
                // 'datas' => Alterpart::where('status', 2)->orWhere('status', 1)->get(),
            ]);
    }

    public function create()
    {
        return view('master.brand.create', [
            'data'  => Brand::get(),
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_brand' => 'unique:t_brand',
             ]);
        $store = Brand::create($request->all());
        return redirect()->route('brand')->with('message', 'Success, Data Berhasil Ditambahkan');
    }

    public function edit($id)
    {
        $data = Brand::where('id',$id)->first();
        return view('master.brand.edit',compact('data'));
    }

    public function update(Request $request,$id)
    {
        $edit = Brand::where('id', $id)->update([
            'kode_brand'     => $request->kode_brand,
            'nama_brand'     => $request->nama_brand,
        ]);
        return redirect('brand')->with('message', 'Success, Data Berhasil Diperbarui');
    }

    public function delete($id){
        $delete = Brand::where('id',$id)->delete();

        if ($delete) {
            return redirect()->route('brand')->with('message','Berhasil Terhapus');
        }else{
            return redirect()->route('brand')->with('danger','Gagal Terhapus');
        }
    }
}
