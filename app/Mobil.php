<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobil extends Model
{
    protected $table = 't_mobil';
    public $timestamps = false;

    protected $guarded = [];
}
