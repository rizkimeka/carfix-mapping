<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapping extends Model
{
    protected $table = 't_mapping';
    public $timestamps = false;

    protected $guarded = [];
}
