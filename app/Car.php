<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $table = 't_car';
    public $timestamps = false;

    protected $guarded = [];

    public function part()
    {
        return $this->hasOne('App\Part', 'kode_mobil', 'kode_mobil');
    }
}
