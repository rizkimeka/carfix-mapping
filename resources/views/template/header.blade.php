<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title')</title>

  <!-- Styles -->
  <link href="{{ asset('dist/modules/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('dist/modules/fontawesome/css/all.min.css') }}" rel="stylesheet">
  <link href="{{ asset('dist/modules/bootstrap-social/bootstrap-social.css') }}" rel="stylesheet">
  <link href="{{ asset('dist/modules/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/izitoast/css/iziToast.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/select2/dist/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/jquery-selectric/selectric.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/dropify/dist/css/dropify.css') }}">
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- CSS Libraries -->

  <link rel="stylesheet" href="{{ asset('dist/modules/datatables/datatables.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
  <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">

  <link href="{{ asset('dist/css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('dist/css/components.css') }}" rel="stylesheet">
  <link href="{{ asset('dist/css/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('dist/css/yearpicker.css') }}" rel="stylesheet">

  <script src="{{ asset('dist/modules/jquery.min.js') }}"></script>
</head>

<body>
