@if(session('message'))
	<div class="alert alert-success alert-dismissible fade-show" role="alert">
		{{ session('message') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidde="true">&times;</span>
		</button>
	</div>
@endif

@if(session('danger'))
	<div class="alert alert-danger alert-dismissible fade-show" role="alert">
		{{ session('danger') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidde="true">&times;</span>
		</button>
	</div>
@endif

@if(session('CustomMessage'))
	<div class="alert alert-{{session('type')}} alert-dismissible fade-show" role="alert">
		{{ session('CustomMessage') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidde="true">&times;</span>
		</button>
	</div>
@endif

@if(session('info'))
	<div class="alert alert-info">{{ session('info') }}</div>
@endif

@if($errors->any())
	@foreach($errors->all() as $error)
		<div class="alert alert-danger">{{ $error }}</div>
	@endforeach
@endif