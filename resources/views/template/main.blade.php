@include('template.header')
  <!-- Side & navbar -->
  @include('template.navbar')


  <!-- Main Content -->
  <div class="main-content">
    <section class="section">

      @yield('content')
    </section>
  </div>
  <footer class="main-footer">
    <div class="footer-left">
      Copyright &copy; 2019 <div class="bullet"></div> Design By <a href="https://carfix.co.id/">Carfix</a>
    </div>
    <div class="footer-right">
      v1.1
    </div>
  </footer>

  @include('template.footer')
