  <!-- General JS Scripts -->
  <script src="{{ asset('dist/modules/jquery.min.js') }}"></script>
  <script src="{{ asset('dist/modules/popper.js') }}"></script>
  <script src="{{ asset('dist/modules/tooltip.js') }}"></script>
  <script src="{{ asset('dist/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('dist/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('dist/modules/moment.min.js') }}"></script>

  <!-- JS Libraies -->
  <script src="{{ asset('dist/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script src="{{ asset('dist/modules/izitoast/js/iziToast.min.js') }}"></script>
  <script src="{{ asset('dist/modules/sweetalert/sweetalert.min.js') }}"></script>

  <script src="{{ asset('dist/js/stisla.js') }}"></script>

    <script src="{{ asset('dist/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('dist/modules/select2/dist/js/select2.full.min.js') }}"></script>

    <script src="{{ asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('dist/modules/jquery-ui/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('dist/js/page/modules-datatables.js') }}"></script>

  <!-- Page Specific JS File -->

  <!-- Template JS File -->
  <script src="{{ asset('dist/js/scripts.js') }}"></script>
  <script src="{{ asset('dist/js/custom.js') }}"></script>
  {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> --}}
  <script src="{{ asset('dist/js/yearpicker.js') }}" async></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#yearpicker').yearpicker();
      });
    </script>
  <script>
    $(document).ready(function(){
        $('.coba').on('click',function(e){
            e.preventDefault();
            var title = $(this).attr('title');
            console.log(title);
        })
    })
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
    });
  </script>
  <script>
        $(document).ready(function() {
          $('#example1').DataTable({
            dom: 'Bfrtip',
            buttons: [
              { extend: 'copy', className: 'btn-primary' },{ extend: 'csv', className: 'btn-info' },{ extend: 'excel', className: 'btn-success' },{ extend: 'pdf', className: 'btn-danger' },{ extend: 'print', className: 'btn-warning' }
            ]
          });
        });
  </script>
  @yield('script')
  <script src="{{asset('dist/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('dist/js/dataTables.bootstrap4.min.js')}}"></script>
</body>
</html>
