@php
  $route = Route::currentRouteName();
@endphp

<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="/home">Carfix Mapping</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="/home">CM</a>
    </div>
      <ul class="sidebar-menu">
            <li class="menu-header">DASHBOARD</li>
            <li class=""><a class="nav-link" href="/home">
              <i class="fas fa-home"></i> <span>Dashboard</span></a>
            </li>
            @if(Auth::user()->hak_akses == 1)
            <li class="menu-header">ADMIN</li>
            <li class="dropdown {{ $route == 'job' || $route == 'job.create' || $route == 'job.edit' || $route == 'brand' || $route == 'brand.create' || $route == 'brand.edit' || $route == 'kelas' || $route == 'kelas.create' || $route == 'kelas.edit' || $route == 'mobil' || $route == 'mobil.create' || $route == 'mobil.edit' ? 'active' : '' }}">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-columns"></i><span>Master Data</span></a>
              <ul class="dropdown-menu">
                <li class="{{ $route == 'job' || $route == 'job.create' || $route == 'job.edit' ? 'active' : '' }}"><a class="nav-link" href="{{ route('job') }}">
                    <i class="fas fa-tasks"></i> <span>Data Job</span></a>
                </li>
              </ul>
              <ul class="dropdown-menu">
                <li class="{{ $route == 'brand' || $route == 'brand.create' || $route == 'brand.edit' ? 'active' : '' }}"><a class="nav-link" href="{{ route('brand') }}">
                    <i class="fas fa-tags"></i> <span>Data Brand</span></a>
                </li>
              </ul>
              <ul class="dropdown-menu">
                <li class="{{ $route == 'kelas' || $route == 'kelas.create' || $route == 'kelas.edit' ? 'active' : '' }}"><a class="nav-link" href="{{ route('kelas') }}">
                    <i class="fas fa-graduation-cap"></i> <span>Data Kelas</span></a>
                </li>
              </ul>
              <ul class="dropdown-menu">
                <li class="{{ $route == 'mobil' || $route == 'mobil.create' || $route == 'mobil.edit' ? 'active' : '' }}"><a class="nav-link" href="{{ route('mobil') }}">
                    <i class="fas fa-car"></i> <span>Data Mobil</span></a>
                </li>
              </ul>
            </li>
            <li class="{{ $route == 'part' || $route == 'part.create' || $route == 'part.edit' ? 'active' : '' }}"><a class="nav-link" href="{{ route('part') }}">
                <i class="fas fa-tag"></i> <span>Mapping Input</span></a>
            </li>
            @endif
            @if(Auth::user()->hak_akses == 2)
            <li class="menu-header">USER</li>
            <li class="{{ $route == 'part' || $route == 'part.create' ? 'active' : '' }}"><a class="nav-link" href="{{ route('part') }}">
                    <i class="fas fa-edit"></i> <span>Input Part</span></a>
            </li>
            @endif
      </ul>
    <!-- <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
      <a href="#" class="btn bg-white btn-lg btn-block btn-icon-split">
        <i class="fas fa-rocket"></i> Contact Developer
      </a>
    </div>         -->
  </aside>
</div>
