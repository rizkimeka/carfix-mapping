@extends('template.main')

@section('title','Edit Part')

@section('content')
<div class="section-body">
    <h2 class="section-title">Edit Part</h2>
    <p class="section-lead">Halaman untuk edit part barang</p>
    @if(Auth::user()->hak_akses == 1)
    @include('template.alert')
    <div class="card">
        <form action="{{ route('part.update',$data['id']) }}" method="post">
            @csrf @method('patch')
          <div class="card-header">
            <h4>Part Data <a href="{{ route('part') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
          </div>
          <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
            <div class="form-group row">
              <label class="col-12 col-md-2 col-form-label text-md-right">Kode Part</label>
              <div class="col-sm-12 col-md-8" id="kode_part">
                  <input type="text" class="form-control" name="kode_part" readonly="" value="{{ $data->kode_part }}">
              </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-md-2 col-form-label text-md-right">Nama Part</label>
              <div class="col-sm-12 col-md-8" id="part">
                  <input type="text" class="form-control" name="nama_part" required="" value="{{ $data->nama_part }}">
              </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-md-2 col-form-label text-md-right">Brand Part</label>
              <div class="col-sm-12 col-md-8" id="brand">
                  <input type="text" class="form-control" name="brand" required="" value="{{ $data->brand }}">
              </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-md-2 col-form-label text-md-right">Nama Job</label>
              <div class="col-sm-12 col-md-8">
                  <select name="nama_job" id="nama_job" class="form-control select2" required="" onchange="job()">
                      <option value=""></option>
                      @foreach ($job as $e)
                      <option value="{{$e->nama_job}}"  {{ $e->nama_job == $data->nama_job ? 'selected':'' }}>{{$e->nama_job}}</option>
                      @endforeach
                  </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-md-2 col-form-label text-md-right">Kode Job</label>
              <div class="col-sm-12 col-md-8">
                  <select name="kode_job" id="kode_job" class="form-control select2" required="">
                      <option value=""></option>
                      @foreach ($job as $e)
                      <option value="{{$e->kode_job}}"  {{ $e->kode_job == $data->kode_job ? 'selected':'' }}>{{$e->kode_job}}</option>
                      @endforeach
                  </select>
              </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
              <label class="col-12 col-md-2 col-form-label text-md-right">Kelas</label>
              <div class="col-sm-12 col-md-8">
                  <select name="kelas_job" id="kelas" class="form-control" required="">
                      <option value=""></option>
                      <option value="Economy" {{ $data->kelas_job == 'Economy' ? 'selected':'' }}>Economy</option>
                      <option value="Premium"  {{ $data->kelas_job == 'Premium' ? 'selected':'' }}>Premium</option>
                      <option value="Ultimate"  {{ $data->kelas_job == 'Ultimate' ? 'selected':'' }}>Ultimate</option>
                  </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-md-2 col-form-label text-md-right">Merek Mobil</label>
                  <div class="col-sm-12 col-md-8">
                      <select name="merek" id="merek" class="form-control select2" required="" onchange="ada()">
                          <option value=""></option>
                          @foreach ($mobil as $e)
                          <option value="{{$e->merek}}"  {{ $e->merek == $data->merek ? 'selected':'' }}>{{$e->merek}}</option>
                          @endforeach
                      </select>
                  </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-md-2 col-form-label text-md-right">Model Mobil</label>
              <div class="col-sm-12 col-md-8">
                  <select name="model" id="model" class="form-control select2" required="" onchange="varian()">
                      <option value=""></option>
                      @foreach ($model as $e)
                      <option value="{{$e->model}}"  {{ $e->model == $data->model ? 'selected':'' }}>{{$e->model}}</option>
                      @endforeach
                  </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-md-2 col-form-label text-md-right">Tipe Mobil</label>
              <div class="col-sm-12 col-md-8">
                  <select name="type" id="type" class="form-control select2" required="">
                      <option value=""></option>
                      @foreach ($varian as $e)
                      <option value="{{$e->varian}}"  {{ $e->varian == $data->type ? 'selected':'' }}>{{$e->varian}}</option>
                      @endforeach
                  </select>
              </div>
            </div>

            <div class="form-group row" id="kdm">
                <input type="hidden" class="form-control" name="kode_mobil" required="">
            </div>

            <div class="form-group row">
                  <label class="col-12 col-md-2 col-form-label text-md-right">Tahun</label>
                  <div class="col-sm-12 col-md-8" id="tahun">
                      <input type="number" class="form-control" id="yearpicker" placeholder="YYYY" min="1950" max="2020" maxlength="4" name="tahun" required="" value="{{ $data->tahun }}">
                  </div>
            </div>

            {{-- <div class="form-group row">
              <label class="col-12 col-md-2 col-form-label text-md-right">Quantity</label>
              <div class="col-sm-12 col-md-8">
                  <input type="text" class="form-control" name="quantity" required="" value="{{ $data->quantity }}">
                  <input type="hidden" name="status" value="1">
              </div>
            </div>
            <small id="formatUang" style="text-align:center;" class="form-text text-muted"></small>
            <div class="form-group row">
                <label class="col-12 col-md-2 col-form-label text-md-right">Harga</label>
                <div class="col-sm-12 col-md-8">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Rp.</span>
                    <input type="number" class="form-control" name="harga" onkeyup="formatRupiah(this.value,'formatUang')" value="{{ $data->harga }}">
                    </div>
                </div>
            </div> --}}
             <div class="card-footer text-right">
                <a href="{{ route('part') }}" class="btn btn-danger ml-2">Cancel</a>
                <button class="btn btn-success">Submit</button>
             </div>
            </div>
            </div>
          </div>
        </form>
      </div>
@endif
@if(Auth::user()->hak_akses == 2)
    @include('template.alert')
    <div class="card">
            <form action="{{ route('part.update',$data['id']) }}" method="post">
                    @csrf @method('patch')
          <div class="card-header">
            <h4>Part Data <a href="{{ route('part') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
          </div>
          <div class="card-body">
                <div class="row">
                        <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-12 col-md-2 col-form-label text-md-right">Kode Part</label>
                  <div class="col-sm-12 col-md-8" id="kode_part">
                      <input type="text" class="form-control" name="kode_part" readonly="" value="{{ $data->kode_part }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-12 col-md-2 col-form-label text-md-right">Nama Part</label>
                  <div class="col-sm-12 col-md-8" id="part">
                      <input type="text" class="form-control" name="nama_part" required="" value="{{ $data->nama_part }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-12 col-md-2 col-form-label text-md-right">Brand Part</label>
                  <div class="col-sm-12 col-md-8" id="brand">
                      <input type="text" class="form-control" name="brand" required="" value="{{ $data->brand }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-12 col-md-2 col-form-label text-md-right">Nama Job</label>
                  <div class="col-sm-12 col-md-8">
                      <select name="nama_job" id="nama_job" class="form-control select2" required="" onchange="job()">
                          <option value=""></option>
                          @foreach ($job as $e)
                          <option value="{{$e->nama_job}}"  {{ $e->nama_job == $data->nama_job ? 'selected':'' }}>{{$e->nama_job}}</option>
                          @endforeach
                      </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-12 col-md-2 col-form-label text-md-right">Kode Job</label>
                  <div class="col-sm-12 col-md-8">
                      <select name="kode_job" id="kode_job" class="form-control select2" required="">
                          <option value=""></option>
                          @foreach ($job as $e)
                          <option value="{{$e->kode_job}}"  {{ $e->kode_job == $data->kode_job ? 'selected':'' }}>{{$e->kode_job}}</option>
                          @endforeach
                      </select>
                  </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-12 col-md-2 col-form-label text-md-right">Kelas</label>
                  <div class="col-sm-12 col-md-8">
                      <select name="kelas_job" id="kelas" class="form-control" required="">
                          <option value=""></option>
                          <option value="Economy" {{ $data->kelas_job == 'Economy' ? 'selected':'' }}>Economy</option>
                          <option value="Premium"  {{ $data->kelas_job == 'Premium' ? 'selected':'' }}>Premium</option>
                          <option value="Ultimate"  {{ $data->kelas_job == 'Ultimate' ? 'selected':'' }}>Ultimate</option>
                      </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-12 col-md-2 col-form-label text-md-right">Merek Mobil</label>
                      <div class="col-sm-12 col-md-8">
                          <select name="merek" id="merek" class="form-control select2" required="" onchange="ada()">
                              <option value=""></option>
                              @foreach ($mobil as $e)
                              <option value="{{$e->merek}}"  {{ $e->merek == $data->merek ? 'selected':'' }}>{{$e->merek}}</option>
                              @endforeach
                          </select>
                      </div>
                </div>

                <div class="form-group row">
                  <label class="col-12 col-md-2 col-form-label text-md-right">Model Mobil</label>
                  <div class="col-sm-12 col-md-8">
                      <select name="model" id="model" class="form-control select2" required="" onchange="varian()">
                          <option value=""></option>
                          @foreach ($model as $e)
                          <option value="{{$e->model}}"  {{ $e->model == $data->model ? 'selected':'' }}>{{$e->model}}</option>
                          @endforeach
                      </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-12 col-md-2 col-form-label text-md-right">Tipe Mobil</label>
                  <div class="col-sm-12 col-md-8">
                      <select name="type" id="type" class="form-control select2" required="">
                          <option value=""></option>
                          @foreach ($varian as $e)
                          <option value="{{$e->varian}}"  {{ $e->varian == $data->type ? 'selected':'' }}>{{$e->varian}}</option>
                          @endforeach
                      </select>
                  </div>
                </div>

                <div class="form-group row" id="kdm">
                    <input type="hidden" class="form-control" name="kode_mobil" required="">
                </div>

                <div class="form-group row">
                      <label class="col-12 col-md-2 col-form-label text-md-right">Tahun</label>
                      <div class="col-sm-12 col-md-8" id="tahun">
                          <input type="number" class="form-control" id="yearpicker" placeholder="YYYY" min="1950" max="2020" maxlength="4" name="tahun" required="" value="{{ $data->tahun }}">
                      </div>
                </div>

                {{-- <div class="form-group row">
                  <label class="col-12 col-md-2 col-form-label text-md-right">Quantity</label>
                  <div class="col-sm-12 col-md-8">
                      <input type="text" class="form-control" name="quantity" required="" value="{{ $data->quantity }}">
                      <input type="hidden" name="status" value="1">
                  </div>
                </div>
                <small id="formatUang" style="text-align:center;" class="form-text text-muted"></small>
                <div class="form-group row">
                    <label class="col-12 col-md-2 col-form-label text-md-right">Harga</label>
                    <div class="col-sm-12 col-md-8">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp.</span>
                        <input type="number" class="form-control" name="harga" onkeyup="formatRupiah(this.value,'formatUang')" value="{{ $data->harga }}">
                        </div>
                    </div>
                </div> --}}
                 <div class="card-footer text-right">
                    <a href="{{ route('part') }}" class="btn btn-danger ml-2">Cancel</a>
                    <button class="btn btn-success">Submit</button>
                 </div>
            </div>
              </div>
          </div>
        </form>
      </div>
@endif
  </div>

  <script>
      function formatRupiah(angka, target) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            document.getElementById(target).innerHTML = 'Rp. ' + rupiah;
        }
  </script>
  <script>
      function job() {
          var cek = $('#nama_part').val();
        //   console.log(cek)
          var selectJob = $('#kode_job');

          $.ajax({
            url: "{{ url('getjob')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                // console.log(data)
                selectJob.empty()
                1 // $('select[name="id_Job"]').empty('');
                selectJob.append('<option value=""></option>');
                $.each(data, function(key, value) {
                    selectJob.append('<option value="' + value.kode_job + '">' + value.kode_job + '</option>');
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }

  function ada() {
          var cek = $('#merek').val();
          console.log(cek)
          var selectMobil = $('#model');

          $.ajax({
            url: "{{ url('getmerek')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                // console.log(data)
                selectMobil.empty()
                1 // $('select[name="id_Mobil"]').empty('');
                selectMobil.append('<option value=""></option>');
                $.each(data, function(key, value) {
                    selectMobil.append('<option value="' + value.model + '">' + value.model + '</option>');
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }
    function varian() {
          var cek = $('#model').val();
        //   console.log(cek)
          var selectMobil = $('#type');
          var selectKode = $('#kdm');

          $.ajax({
            url: "{{ url('getmodel')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                // console.log(data)
                selectMobil.empty()
                1 // $('select[name="id_Mobil"]').empty('');
                selectMobil.append('<option value=""></option>');
                $.each(data, function(key, value) {
                    selectMobil.append('<option value="' + value.varian + '">' + value.varian + '</option>');
                    selectKode.append('<input type="hidden" name="kode_mobil" value="' + value.kode_mobil + '">');
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }
    </script>
@endsection
