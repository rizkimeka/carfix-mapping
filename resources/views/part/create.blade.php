@extends('template.main')

@section('title','Add Part')

@section('content')
<div class="section-body">
    <h2 class="section-title">Input Part</h2>
    <p class="section-lead">Halaman untuk input part</p>
    @if(Auth::user()->hak_akses == 1)
    @include('template.alert')
    <div class="card">
      <form action="{{ route('part.store') }}" method="post">
        @csrf
        <div class="card-header">
          <h4>Part Data <a href="{{ route('part') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
        </div>
        <div class="card-body">
          {{-- <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Kode Part</label>
            <div class="input-group mb-3 col-sm-12 col-md-8" id="kode">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pilih Kode Part</button>
                  <div class="dropdown-menu">
                    <select name="kode_part" id="kode_part" class="form-control select2" onchange="check()">
                        <option value="">-Part-</option>
                        @foreach ($datas as $data)
                        <option class="dropdown-item" value="{{$data->kode_part}}">{{$data->kode_part}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <input type="text" class="form-control" name="kode_part" required="">
            </div>
          </div> --}}
          <div class="row">
                <div class="col-md-6">
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Kode Part</label>
            <div class="col-sm-12 col-md-8" id="kode_part">
                <input type="text" class="form-control" name="kode_part" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Nama Part</label>
            <div class="col-sm-12 col-md-8" id="part">
                <input type="text" class="form-control" name="nama_part" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Brand Part</label>
            <div class="col-sm-12 col-md-8" id="brand">
                <input type="text" class="form-control" name="brand" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Nama Job</label>
            <div class="col-sm-12 col-md-8">
                <select name="nama_job" id="nama_job" class="form-control select2" required="" onchange="job()">
                    <option value=""></option>
                    @foreach ($job as $e)
                    <option value="{{$e->nama_job}}">{{$e->nama_job}}</option>
                    @endforeach
                </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Kode Job</label>
            <div class="col-sm-12 col-md-8">
                <select name="kode_job" id="kode_job" class="form-control select2" required="">
                    <option value=""></option>
                    @foreach ($job as $e)
                    <option value="{{$e->kode_job}}">{{$e->kode_job}}</option>
                    @endforeach
                </select>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Kelas</label>
            <div class="col-sm-12 col-md-8">
                <select name="kelas_job" id="kelas" class="form-control" required="">
                    <option value=""></option>
                    <option value="Economy">Economy</option>
                    <option value="Premium">Premium</option>
                    <option value="Ultimate">Ultimate</option>
                </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Merek Mobil</label>
                <div class="col-sm-12 col-md-8">
                    <select name="merek" id="merek" class="form-control select2" required="" onchange="ada()">
                        <option value=""></option>
                        @foreach ($mobil as $e)
                        <option value="{{$e->merek}}">{{$e->merek}}</option>
                        @endforeach
                    </select>
                </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Model Mobil</label>
            <div class="col-sm-12 col-md-8">
                <select name="model" id="model" class="form-control select2" required="" onchange="varian()">
                    <option value=""></option>
                    @foreach ($model as $e)
                    <option value="{{$e->model}}">{{$e->model}}</option>
                    @endforeach
                </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Tipe Mobil</label>
            <div class="col-sm-12 col-md-8">
                <select name="type" id="type" class="form-control select2" required="">
                    <option value=""></option>
                    @foreach ($varian as $e)
                    <option value="{{$e->varian}}">{{$e->varian}}</option>
                    @endforeach
                </select>
            </div>
          </div>

          <div class="form-group row" id="kdm">
                <input type="hidden" class="form-control" name="kode_mobil" required="">
          </div>

          <div class="form-group row">
                <label class="col-12 col-md-2 col-form-label">Tahun</label>
                <div class="col-sm-12 col-md-8" id="tahun">
                    <input type="number" class="form-control" id="yearpicker" placeholder="YYYY" min="1950" max="2020" maxlength="4" name="tahun" required="">
                </div>
          </div>

          {{-- <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Quantity</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="quantity" required="">
                <input type="hidden" name="status" value="2">
            </div>
          </div>
          <small id="formatUang" style="text-align:center;" class="form-text text-muted"></small>
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label text-md-right">Harga</label>
            <div class="col-sm-12 col-md-8">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp.</span>
                    <input type="number" class="form-control" name="harga" onkeyup="formatRupiah(this.value,'formatUang')">
                </div>
            </div>
          </div> --}}
          <div class="card-footer text-right">
                <a href="{{ route('part') }}" class="btn btn-danger ml-2">Cancel</a>
                <button class="btn btn-success">Submit</button>
          </div>
        </div>
    </div>
    </div>
      </form>
    </div>
@endif
@if(Auth::user()->hak_akses == 2)
    @include('template.alert')
    <div class="card">
        <form action="{{ route('part.store') }}" method="post">
          @csrf
          <div class="card-header">
            <h4>Part Data <a href="{{ route('part') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="col-md-6">
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Kode Part</label>
            <div class="col-sm-12 col-md-8" id="kode_part">
                <input type="text" class="form-control" name="kode_part" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Nama Part</label>
            <div class="col-sm-12 col-md-8" id="part">
                <input type="text" class="form-control" name="nama_part" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Brand Part</label>
            <div class="col-sm-12 col-md-8" id="brand">
                <input type="text" class="form-control" name="brand" required="">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Nama Job</label>
            <div class="col-sm-12 col-md-8">
                <select name="nama_job" id="nama_job" class="form-control select2" required="" onchange="job()">
                    <option value=""></option>
                    @foreach ($job as $e)
                    <option value="{{$e->nama_job}}">{{$e->nama_job}}</option>
                    @endforeach
                </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Kode Job</label>
            <div class="col-sm-12 col-md-8">
                <select name="kode_job" id="kode_job" class="form-control select2" required="">
                    <option value=""></option>
                    @foreach ($job as $e)
                    <option value="{{$e->kode_job}}">{{$e->kode_job}}</option>
                    @endforeach
                </select>
            </div>
          </div>
        </div>
        <div class="col-md-6">
        <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Kelas</label>
            <div class="col-sm-12 col-md-8">
                <select name="kelas_job" id="kelas" class="form-control" required="">
                    <option value=""></option>
                    <option value="Economy">Economy</option>
                    <option value="Premium">Premium</option>
                    <option value="Ultimate">Ultimate</option>
                </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Merek Mobil</label>
                <div class="col-sm-12 col-md-8">
                    <select name="merek" id="merek" class="form-control select2" required="" onchange="ada()">
                        <option value=""></option>
                        @foreach ($mobil as $e)
                        <option value="{{$e->merek}}">{{$e->merek}}</option>
                        @endforeach
                    </select>
                </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Model Mobil</label>
            <div class="col-sm-12 col-md-8">
                <select name="model" id="model" class="form-control select2" required="" onchange="varian()">
                    <option value=""></option>
                    @foreach ($model as $e)
                    <option value="{{$e->model}}">{{$e->model}}</option>
                    @endforeach
                </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Tipe Mobil</label>
            <div class="col-sm-12 col-md-8">
                <select name="type" id="type" class="form-control select2" required="">
                    <option value=""></option>
                    @foreach ($varian as $e)
                    <option value="{{$e->varian}}">{{$e->varian}}</option>
                    @endforeach
                </select>
            </div>
          </div>

          <div class="form-group row" id="kdm">
                <input type="hidden" class="form-control" name="kode_mobil" required="">
          </div>

          <div class="form-group row">
                <label class="col-12 col-md-2 col-form-label">Tahun</label>
                <div class="col-sm-12 col-md-8" id="tahun">
                    <input type="number" class="form-control" id="yearpicker" placeholder="YYYY" min="1950" max="2020" maxlength="4" name="tahun" required="">
                </div>
          </div>

          {{-- <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Quantity</label>
            <div class="col-sm-12 col-md-8">
                <input type="text" class="form-control" name="quantity" required="">
                <input type="hidden" name="status" value="2">
            </div>
          </div>
          <small id="formatUang" style="text-align:center;" class="form-text text-muted"></small>
          <div class="form-group row">
            <label class="col-12 col-md-2 col-form-label">Harga</label>
            <div class="col-sm-12 col-md-8">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp.</span>
                    <input type="number" class="form-control" name="harga" onkeyup="formatRupiah(this.value,'formatUang')">
                </div>
            </div>
          </div> --}}
            </div>
            </div>
            <div class="card-footer text-right">
                    <a href="{{ route('part') }}" class="btn btn-danger ml-2">Cancel</a>
                    <button class="btn btn-success">Submit</button>
            </div>
        </div>
        </form>
      </div>
@endif
  </div>
  <script>
        function formatRupiah(angka, target) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            document.getElementById(target).innerHTML = 'Rp. ' + rupiah;
        }
    </script>
    <script>
        function check() {
        //var count = $('#kode_part option:selected').length;
        var cek = $('#kode_part').val();
        var selectPart = $('#part');
        var selectBrand = $('#brand');
        var selectKode = $('#kode');
        var selectMerek = $('#merek');
        var selectMobil = $('#model');
        var selectTahun = $('#tahun');
        var selectEta = $('#eta');

        $.ajax({
            url: "{{ url('getpart')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                //console.log(data)
                selectPart.empty()
                selectBrand.empty()
                selectMerek.empty()
                selectMobil.empty()
                selectTahun.empty()
                selectEta.empty()
                selectKode.empty()
                1 // $('select[name="id_Mobil"]').empty('');
                // selectMobil.append('<option value="">--Pilih Model--</option>');
                $.each(data, function(key, value) {
                    selectKode.append('<input class="form-control" name="kode_part" readonly value="' + value.kode_part + '">');
                    selectPart.append('<input class="form-control" name="nama_part" value="' + value.nama_part + '">');
                    selectBrand.append('<input class="form-control" name="brand" value="' + value.brand + '">');
                    selectMerek.append('<option value="' + value.merek + '">' + value.merek + '</option>');
                    selectMobil.append('<option value="' + value.model + '">' + value.model + '</option>');
                    selectTahun.append('<input class="form-control" name="tahun" min="1950" max="2020" maxlength="4" value="' + value.tahun + '">');
                    selectEta.append('<input type="date" class="form-control" name="eta" value="' + value.eta + '">');
                    // console.log(value.model)
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }

    function job() {
          var cek = $('#nama_job').val();
        //   console.log(cek)
          var selectJob = $('#kode_job');

          $.ajax({
            url: "{{ url('getjob')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                // console.log(data)
                selectJob.empty()
                1 // $('select[name="id_Job"]').empty('');
                // selectJob.append('<option value=""></option>');
                $.each(data, function(key, value) {
                    selectJob.append('<option value="' + value.kode_job + '" readonly>' + value.kode_job + '</option>');
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }

    function ada() {
          var cek = $('#merek').val();
        //   console.log(cek)
          var selectMobil = $('#model');

          $.ajax({
            url: "{{ url('getmerek')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                // console.log(data)
                selectMobil.empty()
                1 // $('select[name="id_Mobil"]').empty('');
                selectMobil.append('<option value=""></option>');
                $.each(data, function(key, value) {
                    selectMobil.append('<option value="' + value.model + '">' + value.model + '</option>');
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }

    function varian() {
          var cek = $('#model').val();
        //   console.log(cek)
          var selectMobil = $('#type');
          var selectKode = $('#kdm');

          $.ajax({
            url: "{{ url('getmodel')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                // console.log(data)
                selectMobil.empty()
                1 // $('select[name="id_Mobil"]').empty('');
                selectMobil.append('<option value=""></option>');
                $.each(data, function(key, value) {
                    selectMobil.append('<option value="' + value.varian + '">' + value.varian + '</option>');
                    selectKode.append('<input type="hidden" name="kode_mobil" value="' + value.kode_mobil + '">');
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }
    </script>
@endsection
