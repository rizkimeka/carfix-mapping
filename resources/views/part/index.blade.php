@extends('template.main')

@section('title','Input Part')

@section('content')
<div class="section-body">
    <h2 class="section-title">Barang</h2>
    <p class="section-lead">Halaman index barang</p>
    @if(Auth::user()->hak_akses == 1)
    @include('template.alert')
    <div class="card shadow">
        <div class="card-header">
          <h4>Part Data</h4>
          <div class="card-header-action">
            <a href="{{ route('part.create') }}" class="btn btn-info">Add Data <i class="fas fa-plus"></i></a>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="example1">
              <thead>
                <tr>
                  <th><i class="fas fa-th"></i></th>
                  <th>Kode Part</th>
                  <th>Nama Part</th>
                  <th>Brand Part</th>
                  <th>Kode Job</th>
                  <th>Nama Job</th>
                  <th>Kelas Job</th>
                  <th>Merek Mobil</th>
                  <th>Model Mobil</th>
                  <th>Tipe Mobil</th>
                  <th>Tahun</th>
                  {{-- <th>Quantity</th>
                  <th>Harga</th> --}}
                  {{-- <th>Status</th> --}}
                  <th>Action</th>
                  {{-- <th>Tanggal Input</th>
                  <th>Harga</th>
                  <th>Supplier</th> --}}
                  {{-- <th>Action</th> --}}
                </tr>
              </thead>
              <tbody>
              @if(count($data) > 0)
                  @foreach($data as $field)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $field->kode_part }}</td>
                    <td nowrap="">{{ $field->nama_part }}</td>
                    <td>{{ $field->brand }}</td>
                    <td>{{ $field->kode_job }}</td>
                    <td>{{ $field->nama_job }}</td>
                    <td>{{ $field->kelas_job }}</td>
                    <td>{{ $field->merek }}</td>
                    <td>{{ $field->model }}</td>
                    <td>{{ $field->type }}</td>
                    <td>{{ $field->tahun }}</td>
                    {{-- <td>{{ $field->quantity }}</td>
                    <td>{{ $field->harga }}</td> --}}
                    {{-- <td>
                          @if($field->status == 1)
                          <span class="badge badge-info">Sudah Direspon</span>
                          @elseif($field->status == 2)
                          <span class="badge badge-danger">Belum Direspon</span>
                          @elseif($field->status == 3)
                          <span class="badge badge-success">Sudah Dipilih</span>
                          @endif
                    </td> --}}
                    <td nowrap="">
                        <a href="{{ route('part.edit', [$field->id]) }}" class="btn btn-icon btn-success"><i class="fas fa-edit"></i></a>
                        <a onclick="return confirm('Apa anda yakin?')" href="{{ route('part.delete', [$field->id]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                        </a>
                    </td>
                    {{-- <td>{{ $field->tanggal }}</td>
                    <td>{{ $field->harga }}</td>
                    <td>{{ $field->supplier }}</td> --}}
                    {{-- <td>
                      <a href="{{ route('barang.edit', [$field->kode_part]) }}" class="btn btn-icon btn-primary"><i class="fas fa-pen"></i></a>
                      <a onclick="return confirm('Apa anda yakin?')" href="{{ route('barang.delete', [$field->kode_part]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                      </a>
                    </td> --}}
                  </tr>
                  @endforeach
                @else
                  <tr class="text-center">
                    <td colspan="4">No data found</td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
@endif
@if(Auth::user()->hak_akses == 2)
@include('template.alert')
<div class="card shadow">
    <div class="card-header">
      <h4>Part Data</h4>
      <div class="card-header-action">
        <a href="{{ route('part.create') }}" class="btn btn-info">Add Data <i class="fas fa-plus"></i></a>
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="example1">
          <thead>
            <tr>
              <th><i class="fas fa-th"></i></th>
              <th>Kode Part</th>
              <th>Nama Part</th>
              <th>Brand Part</th>
              <th>Kode Job</th>
              <th>Nama Job</th>
              <th>Kelas Job</th>
              <th>Merek Mobil</th>
              <th>Model Mobil</th>
              <th>Tipe Mobil</th>
              <th>Tahun</th>
              {{-- <th>Quantity</th>
              <th>Harga</th> --}}
              <th>Action</th>
              {{-- <th>Tanggal Input</th>
              <th>Harga</th>
              <th>Supplier</th> --}}
              {{-- <th>Action</th> --}}
            </tr>
          </thead>
          <tbody>
          @if(count($data) > 0)
              @foreach($data as $field)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $field->kode_part }}</td>
                <td nowrap="">{{ $field->nama_part }}</td>
                <td>{{ $field->brand }}</td>
                <td>{{ $field->kode_job }}</td>
                <td>{{ $field->nama_job }}</td>
                <td>{{ $field->kelas_job }}</td>
                <td>{{ $field->merek }}</td>
                <td>{{ $field->model }}</td>
                <td>{{ $field->type }}</td>
                <td>{{ $field->tahun }}</td>
                {{-- <td>{{ $field->quantity }}</td>
                <td>{{ $field->harga }}</td> --}}
                {{-- <td>
                      @if($field->status == 1)
                      <span class="badge badge-info">Sudah Direspon</span>
                      @elseif($field->status == 2)
                      <span class="badge badge-danger">Belum Direspon</span>
                      @elseif($field->status == 3)
                      <span class="badge badge-success">Sudah Dipilih</span>
                      @endif
                </td> --}}
                <td nowrap="">
                    <a href="{{ route('part.edit', [$field->id]) }}" class="btn btn-icon btn-success"><i class="fas fa-edit"></i></a>
                    <a onclick="return confirm('Apa anda yakin?')" href="{{ route('part.delete', [$field->id]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                    </a>
                </td>
                {{-- <td>{{ $field->tanggal }}</td>
                <td>{{ $field->harga }}</td>
                <td>{{ $field->supplier }}</td> --}}
                {{-- <td>
                  <a href="{{ route('barang.edit', [$field->kode_part]) }}" class="btn btn-icon btn-primary"><i class="fas fa-pen"></i></a>
                  <a onclick="return confirm('Apa anda yakin?')" href="{{ route('barang.delete', [$field->kode_part]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                  </a>
                </td> --}}
              </tr>
              @endforeach
            @else
              <tr class="text-center">
                <td colspan="4">No data found</td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endif
  </div>
@endsection
