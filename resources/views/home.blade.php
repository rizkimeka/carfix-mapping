@extends('template.main')

@section('title','Report Part Data')

@section('content')
{{-- @if(auth::user()->hak_akses == 1)
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Hi Partman!</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@if(auth::user()->hak_akses == 2)
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Hi Supplier!</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endif --}}
<div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="fas fa-car"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <a href="#">
                            <h4>Total Mobil</h4>
                        </a>
                    </div>
                    <div class="card-body">
                        @php
                        $mobil = DB::table('t_mobil')->count();
                        @endphp
                        {{ $mobil }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                    <i class="fas fa-fw fa-store-alt"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <a href="#">
                            <h4>Total Kelas</h4>
                        </a>
                    </div>
                    <div class="card-body">
                        @php
                        $kelas = DB::table('t_class')->count();
                        @endphp
                        {{ $kelas }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                    <i class="fas fa-fw fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Job</h4>
                    </div>
                    <div class="card-body">
                        @php
                        $job = DB::table('t_job')->count();
                        @endphp
                        {{ $job }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="fas fa-wrench"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <a href="">
                            <h4>Total Part</h4>
                        </a>
                    </div>
                    <div class="card-body">
                        @php
                        $part = DB::table('t_mapping')->count();
                        @endphp
                        {{ $part }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@if(auth::user()->hak_akses == 1)
@include('template.alert')
    <div class="card shadow">
      <div class="card-header">
        <h4>Part Data</h4>
        <div class="card-header-action">
          {{-- <a href="{{ route('barang.create') }}" class="btn btn-info">Add Data <i class="fas fa-plus"></i></a> --}}
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover" id="example1">
            <thead>
              <tr>
                <th><i class="fas fa-th"></i></th>
                <th>Kode Part</th>
                <th>Nama Part</th>
                <th>Brand Part</th>
                <th>Kode Job</th>
                <th>Nama Job</th>
                <th>Kelas Job</th>
                <th>Merek Mobil</th>
                <th>Model Mobil</th>
                <th>Tipe Mobil</th>
                <th>Tahun</th>
                {{-- <th>Quantity</th>
                <th>Harga</th> --}}
                {{-- <th>Status</th> --}}
                {{-- <th>Tanggal Input</th>
                <th>Supplier</th> --}}
                {{-- <th>Action</th> --}}
              </tr>
            </thead>
            <tbody>
            @if(count($data) > 0)
                @foreach($data as $field)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $field->kode_part }}</td>
                  <td nowrap="">{{ $field->nama_part }}</td>
                  <td>{{ $field->brand }}</td>
                  <td>{{ $field->kode_job }}</td>
                  <td>{{ $field->nama_job }}</td>
                  <td>{{ $field->kelas_job }}</td>
                  <td>{{ $field->merek }}</td>
                  <td>{{ $field->model }}</td>
                  <td>{{ $field->type }}</td>
                  <td>{{ $field->tahun }}</td>
                  {{-- <td>{{ $field->quantity }}</td>
                  <td>Rp.{{ $field->harga }}</td> --}}
                  {{-- <td>
                        @if($field->status == 1)
                        <span class="badge badge-info">Sudah Direspon</span>
                        @elseif($field->status == 2)
                        <span class="badge badge-danger">Belum Direspon</span>
                        @elseif($field->status == 3)
                        <span class="badge badge-success">Sudah Dipilih</span>
                        @endif
                  </td> --}}
                  {{-- <td>{{ $field->tanggal }}</td>
                  <td>{{ $field->supplier }}</td> --}}
                  {{-- <td>
                    <a href="{{ route('barang.edit', [$field->kode_part]) }}" class="btn btn-icon btn-primary"><i class="fas fa-pen"></i></a>
                    <a onclick="return confirm('Apa anda yakin?')" href="{{ route('barang.delete', [$field->kode_part]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                    </a>
                  </td> --}}
                </tr>
                @endforeach
              @else
                <tr class="text-center">
                  <td colspan="4">No data found</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
@endif
@endsection
