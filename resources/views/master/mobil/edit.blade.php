@extends('template.main')

@section('title','Edit Mobil Data')

@section('content')
<div class="section-body">
    <h2 class="section-title">Edit Mobil</h2>
    <p class="section-lead">Halaman untuk edit mobil data</p>
    @if(Auth::user()->hak_akses == 1)
    @include('template.alert')
    <div class="card">
        <form action="{{ route('mobil.update',$data['id']) }}" method="post">
            @csrf @method('patch')
          <div class="card-header">
            <h4>Mobil Data <a href="{{ route('mobil') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
          </div>
          <div class="card-body">
            {{-- <div class="row">
                  <div class="col-md-6"> --}}
                    <div class="form-group">
                        <label class="col-12 col-md-2 col-form-label">Kode Mobil</label>
                        <div class="col-sm-12 col-md-6" id="kode_mobil">
                            <input type="text" class="form-control" name="kode_mobil" required="" value="{{ $data->kode_mobil }}">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-12 col-md-2 col-form-label">Merek</label>
                        <div class="col-sm-12 col-md-6" id="merek">
                            <input type="text" class="form-control" name="merek" required="" value="{{ $data->merek }}">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-12 col-md-2 col-form-label">Model</label>
                        <div class="col-sm-12 col-md-6" id="model">
                            <input type="text" class="form-control" name="model" required="" value="{{ $data->model }}">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-12 col-md-2 col-form-label">Type</label>
                        <div class="col-sm-12 col-md-6" id="type">
                            <input type="text" class="form-control" name="type" required="" value="{{ $data->type }}">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-12 col-md-2 col-form-label">Tahun</label>
                        <div class="col-sm-12 col-md-6" id="tahun">
                            <input type="number" class="form-control" name="tahun"  placeholder="YYYY" min="1950" max="2020" maxlength="4" required="" value="{{ $data->tahun }}">
                        </div>
                      </div>

            <div class="card-footer text-center">
                  <a href="{{ route('mobil') }}" class="btn btn-danger ml-2">Cancel</a>
                  <button class="btn btn-success">Submit</button>
            </div>
          {{-- </div>
      </div> --}}
      </div>
        </form>
      </div>
@endif
  </div>
  <script>
      function job() {
          var cek = $('#nama_part').val();
        //   console.log(cek)
          var selectJob = $('#kode_job');

          $.ajax({
            url: "{{ url('getjob')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                // console.log(data)
                selectJob.empty()
                1 // $('select[name="id_Job"]').empty('');
                selectJob.append('<option value=""></option>');
                $.each(data, function(key, value) {
                    selectJob.append('<option value="' + value.kode_job + '">' + value.kode_job + '</option>');
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }
  </script>
@endsection
