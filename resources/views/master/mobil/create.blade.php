@extends('template.main')

@section('title','Add Mobil Data')

@section('content')
<div class="section-body">
    <h2 class="section-title">Input Mobil</h2>
    <p class="section-lead">Halaman untuk input mobil</p>
    @if(Auth::user()->hak_akses == 1)
    @include('template.alert')
    <div class="card">
      <form action="{{ route('mobil.store') }}" method="post">
        @csrf
        <div class="card-header">
          <h4>Part Data <a href="{{ route('mobil') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
        </div>
        <div class="card-body">
          {{-- <div class="row">
                <div class="col-md-6"> --}}
          <div class="form-group">
            <label class="col-12 col-md-2 col-form-label">Kode Mobil</label>
            <div class="col-sm-12 col-md-6" id="kode_mobil">
                <input type="text" class="form-control" name="kode_mobil" required="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-12 col-md-2 col-form-label">Merek</label>
            <div class="col-sm-12 col-md-6" id="merek">
                <input type="text" class="form-control" name="merek" required="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-12 col-md-2 col-form-label">Model</label>
            <div class="col-sm-12 col-md-6" id="model">
                <input type="text" class="form-control" name="model" required="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-12 col-md-2 col-form-label">Type</label>
            <div class="col-sm-12 col-md-6" id="type">
                <input type="text" class="form-control" name="type" required="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-12 col-md-2 col-form-label">Tahun</label>
            <div class="col-sm-12 col-md-6" id="tahun">
                <input type="number" class="form-control" name="tahun"  placeholder="YYYY" min="1950" max="2020" maxlength="4" required="">
            </div>
          </div>

          <div class="card-footer text-center">
                <a href="{{ route('mobil') }}" class="btn btn-danger ml-2">Cancel</a>
                <button class="btn btn-success">Submit</button>
          </div>
        {{-- </div>
    </div> --}}
    </div>
      </form>
    </div>
@endif
  </div>
@endsection
