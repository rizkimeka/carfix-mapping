@extends('template.main')

@section('title','Edit Kelas Data')

@section('content')
<div class="section-body">
    <h2 class="section-title">Edit Kelas</h2>
    <p class="section-lead">Halaman untuk edit kelas data</p>
    @if(Auth::user()->hak_akses == 1)
    @include('template.alert')
    <div class="card">
        <form action="{{ route('kelas.update',$data['id']) }}" method="post">
            @csrf @method('patch')
          <div class="card-header">
            <h4>Part Data <a href="{{ route('kelas') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
          </div>
          <div class="card-body">
            {{-- <div class="row">
                  <div class="col-md-6"> --}}
            <div class="form-group">
              <label class="col-12 col-md-2 col-form-label">Kode Kelas</label>
              <div class="col-sm-12 col-md-6" id="kode_kelas">
                <input type="text" class="form-control" name="kode_kelas" required="" value="{{ $data->kode_kelas }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-12 col-md-2 col-form-label">Nama Kelas</label>
              <div class="col-sm-12 col-md-6" id="kelas">
                  <input type="text" class="form-control" name="nama_kelas" required="" value="{{ $data->nama_kelas }}">
              </div>
            </div>

            <div class="card-footer text-center">
                  <a href="{{ route('kelas') }}" class="btn btn-danger ml-2">Cancel</a>
                  <button class="btn btn-success">Submit</button>
            </div>
          {{-- </div>
      </div> --}}
      </div>
        </form>
      </div>
@endif
  </div>
@endsection
