@extends('template.main')

@section('title','Index Master Brand')

@section('content')
<div class="section-body">
    <h2 class="section-title">Brand</h2>
    <p class="section-lead">Halaman index brand</p>
    @if(Auth::user()->hak_akses == 1)
    @include('template.alert')
    <div class="card shadow">
        <div class="card-header">
          <h4>Job Data</h4>
          <div class="card-header-action">
            <a href="{{ route('brand.create') }}" class="btn btn-info">Add Data <i class="fas fa-plus"></i></a>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="example1">
              <thead>
                <tr>
                  <th><i class="fas fa-th"></i></th>
                  <th>Kode Brand</th>
                  <th>Nama Brand</th>
                  <th>Action</th>
                  {{-- <th>Tanggal Input</th>
                  <th>Harga</th>
                  <th>Supplier</th> --}}
                  {{-- <th>Action</th> --}}
                </tr>
              </thead>
              <tbody>
              @if(count($data) > 0)
                  @foreach($data as $field)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $field->kode_brand }}</td>
                    <td nowrap="">{{ $field->nama_brand }}</td>
                    <td nowrap="">
                        <a href="{{ route('brand.edit', [$field->id]) }}" class="btn btn-icon btn-success"><i class="fas fa-edit"></i></a>
                        <a onclick="return confirm('Apa anda yakin?')" href="{{ route('brand.delete', [$field->id]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                        </a>
                    </td>
                    {{-- <td>{{ $field->tanggal }}</td>
                    <td>{{ $field->harga }}</td>
                    <td>{{ $field->supplier }}</td> --}}
                    {{-- <td>
                      <a href="{{ route('barang.edit', [$field->kode_part]) }}" class="btn btn-icon btn-primary"><i class="fas fa-pen"></i></a>
                      <a onclick="return confirm('Apa anda yakin?')" href="{{ route('barang.delete', [$field->kode_part]) }}" class="btn btn-danger"><i class="menu-icon fa fa-trash"></i>
                      </a>
                    </td> --}}
                  </tr>
                  @endforeach
                @else
                  <tr class="text-center">
                    <td colspan="4">No data found</td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
@endif
  </div>
@endsection
