@extends('template.main')

@section('title','Edit Brand Data')

@section('content')
<div class="section-body">
    <h2 class="section-title">Edit Brand</h2>
    <p class="section-lead">Halaman untuk edit brand data</p>
    @if(Auth::user()->hak_akses == 1)
    @include('template.alert')
    <div class="card">
        <form action="{{ route('brand.update',$data['id']) }}" method="post">
            @csrf @method('patch')
          <div class="card-header">
            <h4>Part Data <a href="{{ route('brand') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
          </div>
          <div class="card-body">
            {{-- <div class="row">
                  <div class="col-md-6"> --}}
            <div class="form-group">
              <label class="col-12 col-md-2 col-form-label">Kode Brand</label>
              <div class="col-sm-12 col-md-6" id="kode_brand">
                <input type="text" class="form-control" name="kode_brand" required="" value="{{ $data->kode_brand }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-12 col-md-2 col-form-label">Nama Brand</label>
              <div class="col-sm-12 col-md-6" id="brand">
                  <input type="text" class="form-control" name="nama_brand" required="" value="{{ $data->nama_brand }}">
              </div>
            </div>

            <div class="card-footer text-center">
                  <a href="{{ route('brand') }}" class="btn btn-danger ml-2">Cancel</a>
                  <button class="btn btn-success">Submit</button>
            </div>
          {{-- </div>
      </div> --}}
      </div>
        </form>
      </div>
@endif
  </div>
@endsection
