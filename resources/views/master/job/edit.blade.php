@extends('template.main')

@section('title','Edit Job Data')

@section('content')
<div class="section-body">
    <h2 class="section-title">Edit Job</h2>
    <p class="section-lead">Halaman untuk edit job data</p>
    @if(Auth::user()->hak_akses == 1)
    @include('template.alert')
    <div class="card">
        <form action="{{ route('job.update',$data['id']) }}" method="post">
            @csrf @method('patch')
          <div class="card-header">
            <h4>Part Data <a href="{{ route('job') }}" class="btn btn-info ml-2"><i class="fas fa-arrow-left"></i> Back</a></h4>
          </div>
          <div class="card-body">
            {{-- <div class="row">
                  <div class="col-md-6"> --}}
            <div class="form-group">
              <label class="col-12 col-md-2 col-form-label">Kode Job</label>
              <div class="col-sm-12 col-md-6" id="kode_job">
                <input type="text" class="form-control" name="kode_job" required="" value="{{ $data->kode_job }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-12 col-md-2 col-form-label">Nama Job</label>
              <div class="col-sm-12 col-md-6" id="job">
                  <input type="text" class="form-control" name="nama_job" required="" value="{{ $data->nama_job }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-12 col-md-2 col-form-label">Flatrate</label>
              <div class="col-sm-12 col-md-6" id="flatrate">
                  <input type="number" class="form-control" name="flatrate" required="" value="{{ $data->flatrate }}">
              </div>
            </div>

            <div class="card-footer text-center">
                  <a href="{{ route('job') }}" class="btn btn-danger ml-2">Cancel</a>
                  <button class="btn btn-success">Submit</button>
            </div>
          {{-- </div>
      </div> --}}
      </div>
        </form>
      </div>
@endif
  </div>
  <script>
      function job() {
          var cek = $('#nama_part').val();
        //   console.log(cek)
          var selectJob = $('#kode_job');

          $.ajax({
            url: "{{ url('getjob')}}" + "/" + cek,
            type: "GET",
            dataType: "json",
            success: function(data) {
                // console.log(data)
                selectJob.empty()
                1 // $('select[name="id_Job"]').empty('');
                selectJob.append('<option value=""></option>');
                $.each(data, function(key, value) {
                    selectJob.append('<option value="' + value.kode_job + '">' + value.kode_job + '</option>');
                    // $('select[name="id_kota"]').attr('disabled',false);
                });
            }
        });
    }
  </script>
@endsection
