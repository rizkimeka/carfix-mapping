@extends('template.blank')

@section('title','Register')

@section('content')
<div class="w-100 bg-white" style="height: 100vh; position: fixed; background: url({{ asset('dist/img/world.jpeg') }}) no-repeat; background-size:cover;">
    <div class="row justify-content-center">
        <div class="">
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    @include('template.alert')
        <div class="card card-primary shadow-lg">
            <div class="card-header"><h4>Register</h4></div>

            <div class="card-body" style="padding-bottom: 0px;">
            <form method="POST" action="{{ route('register') }}">
                @csrf

            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                <label for="name" class="col-form-label">{{ __('Name') }}</label>

                     <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                           <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="password" class="col-form-label">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                </div>

                {{-- <div class="form-group">
                    <label for="hak_akses" class="col-form-label">{{ __('Hak Akses') }}</label>
                        <select name="hak_akses" class="form-control">
                            <option value="1">Admin</option>
                            <option value="2">User</option>
                        </select>
                </div> --}}
            </div>
        </div>
        <div class="form-group">
            <label for="no_hp" class="col-form-label">{{ __('No. Handphone') }}</label>
                <input id="no_hp" type="number" class="form-control @error('no_hp') is-invalid @enderror" name="no_hp" value="{{ old('no_hp') }}" required autocomplete="no_hp">
                    @error('no_hp')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
        </div>
                    {{-- <div class="form-group">
                        <label for="cabang" class="col-form-label">{{ __('Cabang') }}</label>
                            <select name="cabang" class="form-control">
                                    <option value=""></option>
                                    @foreach($cabang as $data)
                                    <option value="{{$data->nama_cabang}}">{{$data->nama_cabang}}</option>
                                    @endforeach
                            </select>
                    </div> --}}

                    <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                        Register
                    </button>
                    <br>
                    @if (Route::has('login'))
                        <center>
                        <a href="{{ route('login') }}">Already have an account?</a>
                        </center>
                    @endif
                    </div>
            </form>
            </div>
        </div>
        </div>
    </div>
</div>

@endsection
