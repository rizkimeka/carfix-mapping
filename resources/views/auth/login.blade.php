@extends('template.blank')

@section('title','Login')

@section('content')
<div class="w-100 bg-white" style="height: 100vh; position: fixed; background: url({{ asset('dist/img/world.jpeg') }}) no-repeat; background-size:cover;">
    <div class="row justify-content-center">
        <div class="col-10 col-sm-8 col-md-6 col-lg-6 col-xl-3">
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    @include('template.alert')
        <div class="card card-primary shadow-lg">
            <div class="card-header"><h4>Login</h4></div>

            <div class="card-body">
            <form method="POST" action="{{ route('login') }} " class="needs-validation">
                @csrf
                <div class="form-group">
                <label for="no_hp">No. Handphone</label>
                <input id="no_hp" type="number" class="form-control {{ $errors->has('no_hp') ? ' is-invalid' : '' }}" name="no_hp" value="{{ old('no_hp') }}" required autofocus>
                @if ($errors->has('no_hp'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('no_hp') }}</strong>
                    </span>
                @endif
                </div>

                <div class="form-group">
                <label for="password" class="control-label">Password</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                </div>

                <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} class="custom-control-input" tabindex="3" id="remember" checked>
                    <label class="custom-control-label" for="remember">Remember Me</label>
                </div>
                </div>

                <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                    Login
                </button>
                <br>
                @if (Route::has('register'))
                    <center>
                    <a href="{{ route('register') }}">Sign up an account?</a>
                    </center>
                @endif
                </div>
            </form>

            </div>
        </div>
        </div>
    </div>
</div>

@endsection
